{if $kilos <= 50 && $supplier["contract_subsidiary"] != ''}
  {if count($sucursales)}
  <div class="row">
    <div class="col-md-10">
      <p>Seleccione su provincia para ver las sucursales disponibles.</p>
    </div>
  </div>
  <hr />
  <div class="provincias-cont" style="margin: 10px 0;">
    <select
      name="provincia"
      id="provincia"
      class="provincias-select address_select form-control"
      style="margin: 10px 0;"
    >
      <option value="-1">Provincias</option>
      {foreach $provincias as $provincia}
      {if $provincia == $prov_selected}
      <option selected value="{$provincia}">{$provincia}</option>
      {else}
      <option value="{$provincia}">{$provincia}</option>
      {/if}
      {/foreach}
    </select>
  </div>
  <input type="hidden" id="selected-correo" value="" />
  <div class="provincias-cont" style="margin: 10px 0;">
    <select
      name="sucursal"
      id="sucursal"
      class="provincias-select address_select form-control"
      style="margin: 10px 0;"
    >
      <option value="-1">Sucursales</option>
      {foreach $sucursales as $key => $val}
      {assign var="datos" value=';'|explode:$val} 
      {if $datos[2] == $suc_selected}
      <option selected value="{$val}">
      {$key}
      </option>
      {else}
      <option value="{$val}">
      {$key}
      </option>
      {/if}
      {/foreach}
    </select>
  </div>
  {else}
  <div style="padding: 20px 0;">
    No hay correos disponibles para el modo de retiro en sucursal.
  </div>
  {/if}
{/if}
<script language="JavaScript" type="text/javascript" src="/js/jquery/jquery-1.11.0.min.js"></script>
<script>
    $(document).ready(function () {
      $("[name*='processCarrier']").click(function(){
        if($(".delivery_option:contains('A sucursal')").find('input[type="radio"]').is(':checked')){
          var suc = $("#sucursal").val();
          if(suc == '-1'){
            alert("Debe seleccionar una sucursal");
            return false;
          }
        }
      });
      if($(".delivery_option:contains('A sucursal')").length > 0){
        if('{$kilos}' > '50' || '{$supplier["contract_subsidiary"]}' == ''){
          $(".delivery_option:contains('A sucursal')").addClass("carrier-price");
        }
      }
      if($(".delivery_option:contains('Estándar a domicilio')").length > 0){
        if('{$kilos}' > '50' || '{$supplier["contract_standar"]}' == ''){
          $(".delivery_option:contains('Estándar a domicilio')").addClass("carrier-price");
        }
      }
      if($(".delivery_option:contains('Urgentes a domicilio')").length > 0){
        if('{$kilos}' > '50' || '{$supplier["contract_urgent"]}' == ''){
          $(".delivery_option:contains('Urgentes a domicilio')").addClass("carrier-price");
        }
      }
      var tablaopt = $(".delivery_options");
      var options = tablaopt.find(".delivery_option");
      options.each(function(){
        var opt = $(this).find(".delivery_option_radio [type=radio]");
        var precio = $(this).find(".delivery_option_price");
        console.log({$id_carrier});
        console.log(opt);
        if({$id_carrier} == opt.val().replace(",","")){
          precio.addClass(" price_show");
        }else{
          precio.addClass(" price_hide");
        }
      });
      if({$id_carrier} != {$id_a_sucursal}){
        $(".hook_extracarrier").removeClass("hook_extracarrier_show");
        $(".hook_extracarrier").addClass("hook_extracarrier_hide");
      }
      $("#provincia").val('{$prov_selected}');
      var provincia = '{$prov_selected}';
      if(provincia == "-1"){
        $("#sucursal").prop('disabled', 'disabled');
      }else{
        $("#sucursal").prop('disabled', false);
      }
      var contenido = provincia.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
      // alert(contenido);
      $("#sucursal option").each(function(){
          var prov_sucursal = this.value.split(';')[1];
          // alert(prov_sucursal);
          if (prov_sucursal != contenido){ //es de otra provincia
            $(this).hide();
          }else{ //es de misma provincia
            $(this).show();
          }
      });
      $('#provincia').on('change', function (e) {
        $("#sucursal").val("-1");
          if(this.value == "-1"){
            $("#sucursal").prop('disabled', 'disabled');
          }else{
            $("#sucursal").prop('disabled', false);
          }
          var contenido = this.value.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
          $("#sucursal option").each(function(){
              var prov_sucursal = $(this).val().split(';')[1];
              if (prov_sucursal != contenido){ //es de otra provincia
                $(this).hide();
              }else{ //es de misma provincia
                $(this).show();
              }
          });
      });
      $('#sucursal').on('change', function (e) {
        var codPostal = $(this).val().split(';')[0];
        var prov_selected = $('#provincia').val();
        var suc_selected = $(this).val().split(';')[2];
        $.ajax({
          type: "post",
          url: "/modules/shippingmodule/ajax.php",
          datatype: "json",
          data: {
              id_carrier: '{$id_a_sucursal}',
              suc_selected: suc_selected,
              prov_selected: prov_selected,
              postcode: codPostal,
              id_supplier: '{$id_supplier}',
              id_cart: '{$id_cart}',
              kilos: '{$kilos}',
              method: 'setRelayPointAndGetCost'
          },

          success: function (response) {
            console.log(response);
            var tablaopt = $(".delivery_options");
            var options = tablaopt.find(".delivery_option");
            options.each(function(){
              var opt = $(this).find(".delivery_option_radio [type=radio]");
              var precio = $(this).find(".delivery_option_price");
              console.log({$id_carrier});
              console.log(opt.val().replace(",",""));
              if({$id_a_sucursal} == opt.val().replace(",","")){
                precio.html(response+" ARS (impuestos incl.)")
                precio.addClass(" price_show");
                precio.removeClass(" price_hide");
              }else{
                precio.html(response+" ARS (impuestos incl.)")
                precio.addClass(" price_hide");
                precio.removeClass(" price_show");
              }
            });
            //location.reload();
          }
        });
      });
      $(".delivery_option_radio [type=radio]").click(function(){
        var unvalor = this.value.split(',')[0];
        if(this.value.split(',')[0] != {$id_a_sucursal}){
          $(".hook_extracarrier").removeClass("hook_extracarrier_show");
          $(".hook_extracarrier").addClass("hook_extracarrier_hide");
          $.ajax({
            type: "post",
            url: "/modules/shippingmodule/ajax.php",
            datatype: "json",
            data: {
                id_carrier: this.value.split(',')[0],
                id_supplier: '{$id_supplier}',
                postcode: '{$address->postcode}',
                kilos: '{$kilos}',
                id_cart: '{$id_cart}',
                method: 'setRelayPointAndGetCost2'
            },

            success: function (response) {
              console.log(response);
              var tablaopt = $(".delivery_options");
              var options = tablaopt.find(".delivery_option");
              options.each(function(){
                var opt = $(this).find(".delivery_option_radio [type=radio]");
                var precio = $(this).find(".delivery_option_price");
                console.log({$id_carrier});
                console.log(opt.val().replace(",",""));
                if(unvalor == opt.val().replace(",","")){
                  precio.html(response+" ARS (tax incl.)")
                  precio.addClass(" price_show");
                  precio.removeClass(" price_hide");
                }else{
                  precio.html(response+" ARS (tax incl.)")
                  precio.addClass(" price_hide");
                  precio.removeClass(" price_show");
                }
              });
              //location.reload();
            }
          });
        }else{
          $(".hook_extracarrier").removeClass("hook_extracarrier_hide");
          $(".hook_extracarrier").addClass("hook_extracarrier_show");
        }
      });
    });
</script>
<style>
.carrier-price{
  display:none !important;
}
.price_show{
  display: !important;
}
.price_hide{
  display:none !important;
}
.hook_extracarrier_show{
  display: !important;
}
.hook_extracarrier_hide{
  display:none !important;
}
</style>