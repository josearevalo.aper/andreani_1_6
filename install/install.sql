/* Tabla que asocia suppliers con información de envioandreani*/
CREATE TABLE IF NOT EXISTS `PREFIX_envioandreani_supplier` (
  `id_supplier`	INT(10) UNSIGNED,
  `user_supplier`	VARCHAR(100),
  `pass_supplier`	VARCHAR(100),
  `number_client`	VARCHAR(50),
  `contract_standar`	VARCHAR(50),
  `contract_urgent`	VARCHAR(50),
  `contract_subsidiary`	VARCHAR(50),
  `address_postcode`	VARCHAR(50),
  `address_city`	VARCHAR(100),
  `address_street`	VARCHAR(100),
  `address_number`	INT(10),
  `id_subsidiary`	INT(10) DEFAULT -1,
  `supplier_name`	VARCHAR(100),
  `supplier_email`	VARCHAR(100),
  UNIQUE KEY `id_supplier` (`id_supplier`),
  FOREIGN KEY (`id_supplier`) REFERENCES `PREFIX_supplier`(`id_supplier`) ON UPDATE CASCADE ON DELETE CASCADE
)	ENGINE=InnoDB	DEFAULT	CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_envioandreani_order` (
  `id_order`	INT(10) UNSIGNED,
  `id_subsidiary`	INT(10),
  `number_contract`	VARCHAR(20),
  UNIQUE KEY `id_order` (`id_order`),
  FOREIGN KEY (`id_order`) REFERENCES `PREFIX_orders`(`id_order`) ON UPDATE CASCADE ON DELETE CASCADE
)	ENGINE=InnoDB	DEFAULT	CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_envioandreani_carrier` (
  `id_carrier`	INT(10) UNSIGNED,
  `name`	VARCHAR(20),
  UNIQUE KEY `id_carrier` (`id_carrier`),
  FOREIGN KEY (`id_carrier`) REFERENCES `PREFIX_carrier`(`id_carrier`) ON UPDATE CASCADE ON DELETE CASCADE
)	ENGINE=InnoDB	DEFAULT	CHARSET=utf8;