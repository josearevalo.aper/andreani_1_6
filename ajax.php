<?php
/**
 * Created by IntelliJ IDEA.
 * User: gus
 * Date: 11/09/16
 * Time: 22:58
 */

use Andreani\Andreani;
use Andreani\Requests\CotizarEnvio;

require_once(dirname(__FILE__) . '/../../config/config.inc.php');
require_once(dirname(__FILE__) . '/../../init.php');

$method = tools::getValue('method');

switch (tools::getValue('method')) {
    /* Establece la sucursal a donde realizar el envio y calcula su costo */
    case 'setRelayPointAndGetCost':

        $id_supplier = tools::getValue('id_supplier');
        $query = 'SELECT * FROM PREFIX_envioandreani_supplier es '
        . 'WHERE es.id_supplier = '.$id_supplier;
        $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
        $proveedor = Db::getInstance()->executeS(trim($query))[0];

        $tarifa = 0;
        $contrato = $proveedor['contract_subsidiary'];

        $url = 'https://api.andreani.com/v1/tarifas';

        $data = array (
            'cpDestino' => tools::getValue('postcode'),
            'contrato' => $contrato,
            'cliente' => $proveedor['number_client'],
            // 'sucursalOrigen' => 'BAR',
            'bultos[0][valorDeclarado]' => null,
            'bultos[0][volumen]' => null,
            'bultos[0][kilos]' => tools::getValue('kilos')
            );
            
            $params = '';
        foreach($data as $key=>$value)
                    $params .= $key.'='.$value.'&';
            
            $params = trim($params, '&');

        $ch = curl_init();

        $cart = new Cart(tools::getValue('id_cart'));
        $cart->id_carrier = tools::getValue('id_carrier');
        // $delivery = explode('"',$cart->delivery_option);
        $cart->delivery_option = 'a:'.$cart->id_shop.':{i:'.$cart->id_address_delivery.';s:3:"'.tools::getValue('id_carrier').',";}';
        $cart->update();

        curl_setopt($ch, CURLOPT_URL, $url.'?'.$params ); //Url together with parameters
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
        curl_setopt($ch, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
        curl_setopt($ch, CURLOPT_HEADER, 0);

        $result = curl_exec($ch);
        curl_close($ch);
        if(isset(json_decode($result)->tarifaConIva)){
            $tarifa = json_decode($result)->tarifaConIva->total;
        }
        Context::getContext()->cookie->__set('id_carrier', tools::getValue('id_carrier'));
        Context::getContext()->cookie->__set('contrato', $contrato);
        Context::getContext()->cookie->__set('shipping_cost', $tarifa);
        Context::getContext()->cookie->__set('prov_selected', tools::getValue('prov_selected'));
        Context::getContext()->cookie->__set('suc_selected', tools::getValue('suc_selected'));

        echo $tarifa;
        break;
    /* Calcula el costo al domicilio */
    case 'setRelayPointAndGetCost2':

        $id_supplier = tools::getValue('id_supplier');
        $query = 'SELECT * FROM PREFIX_envioandreani_supplier es '
        . 'WHERE es.id_supplier = '.$id_supplier;
        $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
        $proveedor = Db::getInstance()->executeS(trim($query))[0];

        Context::getContext()->cookie->__unset('prov_selected');
        Context::getContext()->cookie->__unset('suc_selected');
        $tarifa = 0;

        $query = 'SELECT * FROM PREFIX_carrier c JOIN PREFIX_envioandreani_carrier ec 
        on c.id_carrier = ec.id_carrier '
        . 'WHERE c.id_carrier = '.tools::getValue('id_carrier');
        $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
        $existe = Db::getInstance()->executeS(trim($query))[0];
        
        if($existe['name'] == "Urgentes a domicilio"){
            $contrato = $proveedor['contract_urgent'];
        }else{
            $contrato = $proveedor['contract_standar'];
        }

        $cart = new Cart(tools::getValue('id_cart'));
        $cart->id_carrier = tools::getValue('id_carrier');
        // $delivery = explode('"',$cart->delivery_option);
        $cart->delivery_option = 'a:'.$cart->id_shop.':{i:'.$cart->id_address_delivery.';s:3:"'.tools::getValue('id_carrier').',";}';
        $cart->update();

        $url = 'https://api.andreani.com/v1/tarifas';

        $data = array (
            'cpDestino' => tools::getValue('postcode'),
            'contrato' => $contrato,
            'cliente' => $proveedor['number_client'],
            // 'sucursalOrigen' => 'BAR',
            'bultos[0][valorDeclarado]' => null,
            'bultos[0][volumen]' => null,
            'bultos[0][kilos]' => tools::getValue('kilos')
            );
            
            $params = '';
        foreach($data as $key=>$value)
                    $params .= $key.'='.$value.'&';
            
            $params = trim($params, '&');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url.'?'.$params ); //Url together with parameters
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
        curl_setopt($ch, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
        curl_setopt($ch, CURLOPT_HEADER, 0);

        $result = curl_exec($ch);
        curl_close($ch);
        if(isset(json_decode($result)->tarifaConIva)){
            $tarifa = json_decode($result)->tarifaConIva->total;
        }
        Context::getContext()->cookie->__set('id_carrier', tools::getValue('id_carrier'));
        Context::getContext()->cookie->__set('contrato', $contrato);
        Context::getContext()->cookie->__set('shipping_cost', $tarifa);
        echo $tarifa;
        break;    
    default:
        break;
}
exit;
