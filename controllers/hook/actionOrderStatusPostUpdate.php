<?php
/*
 *
 */

use Andreani\Andreani;
use Andreani\Requests\ConfirmarCompra;

class shippingmoduleActionOrderStatusPostUpdateController
{
    private $file;
    private $module;
    private $context;
    private $_path;

    public function __construct($module, $file, $path)
    {
        $this->file = $file;
        $this->module = $module;
        $this->context = Context::getContext();
        $this->_path = $path;
    }

    public function run($params)
    {
        $id_order = $params['id_order'];

        $query = 'SELECT eo.*, od.product_weight FROM PREFIX_envioandreani_order eo '
        . 'LEFT JOIN PREFIX_order_detail od '
        . 'ON od.id_order = eo.id_order '
        . 'WHERE eo.id_order = '.$id_order;
        $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
        $orden = Db::getInstance()->executeS(trim($query))[0];
        
        $address = new Address($params['cart']->id_address_delivery);
        $customer = new Customer($params['cart']->id_customer);
        $provincia = new State($address->id_state);
        
        $kilos = 0;
        $query = 'SELECT * FROM PREFIX_cart_product cp '
        . 'WHERE cp.id_cart = '.$params['cart']->id;
        $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
        $productos = Db::getInstance()->executeS(trim($query));
        $id_supplier = 0;
        foreach($productos as $producto):
            $query2 = 'SELECT * FROM PREFIX_product p '
            . 'WHERE p.id_product = '.$producto['id_product'];
            $query2 = str_replace('PREFIX_', _DB_PREFIX_, $query2);
            $prods = Db::getInstance()->executeS(trim($query2))[0];
            $id_supplier = $prods['id_supplier'];
            
            if (count($prods) > 0) {
                $kilos = $kilos + ($prods['weight'] * $producto['quantity']);
            }
        endforeach;

        $query = 'SELECT * FROM PREFIX_envioandreani_supplier es '
        . 'WHERE es.id_supplier = '.$id_supplier;
        $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
        $proveedor = Db::getInstance()->executeS(trim($query))[0];

        $url = 'https://api.andreani.com/login';

        $login = base64_encode($proveedor['user_supplier'].':'.$proveedor['pass_supplier']);
        $getRequest = array(
            'Authorization: Basic '.$login
        );
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET"); 
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $getRequest);

        $result = curl_exec($ch);
        //close cURL resource

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $headers = substr($result, 0, $header_size);
        $headers_arr = explode("\r\n", $headers);
        $token = str_replace("X-Authorization-token: ","",$headers_arr[3]);
        curl_close($ch);

        //API URL
        $url = 'https://api.andreani.com/v2/ordenes-de-envio';

        //create a new cURL resource
        $ch = curl_init($url);

        //setup request to send json via POST
        if ($proveedor['id_subsidiary'] == -1) {
            $origen = '{
                "postal": {
                    "codigoPostal": "'.$proveedor['address_postcode'].'",
                    "calle": "'.$proveedor['address_street'].'",
                    "numero": "'.$proveedor['address_number'].'",
                    "localidad": "'.$proveedor['address_city'].'"
                }
            }';
        }else{
            $origen = '{
                "sucursal": {
                    "id": "'.$proveedor['id_subsidiary'].'"
                }
            }';
        }

        if ($orden['id_subsidiary'] == 0) {
            $destino = '{
                "postal": {
                    "codigoPostal": "'.$address->postcode.'",
                    "calle": "'.$address->address1.'",
                    "numero": "Nro: '.$address->address2.'",
                    "localidad": "'.$address->city.'",
                    "pais": "'.$address->country.'"
                }
            }';
        }else{
            $destino = '{
                "sucursal": {
                    "id": "'.$orden['id_subsidiary'].'"
                }
            }';
        }
        $remitente = '{
            "nombreCompleto": "'.$proveedor['supplier_name'].'",
            "email": "'.$proveedor['supplier_email'].'"
        }';
        $destinatario = '[
            {
                "nombreCompleto": "'. $customer->firstname . " " . (isset($customer->lastname) ? $customer->lastname : '') . '",
                "email": "'. $customer->email . '",
                "documentoTipo": "DNI",
                "documentoNumero": "'.$address->dni.'",
                "telefonos": [
                    {
                        "tipo": 2,
                        "numero": "'.$address->phone.'"
                    }
                ]
            }
        ]';
        if($kilos <= 0){
            $kilos = 0.1;
        }
        $bultos = '[
            {
                "kilos": '.$kilos.',
                "volumenCm": 5000
            }
        ]';
        $payload = '{
            "contrato": "'.$orden['number_contract'].'",
            "origen": '.$origen.',
            "destino": '.$destino.',
            "remitente": '.$remitente.',
            "destinatario": '.$destinatario.',
            "bultos": '.$bultos.'
        }';

        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        //set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'x-authorization-token:'.$token));

        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //execute the POST request
        $result = curl_exec($ch);

        //close cURL resource
        curl_close($ch);

        // echo var_dump(json_decode($result)->bultos[0]->linking[0]->contenido);
        // echo json_decode($result)->bultos[0]->linking[0]->contenido;

        
        if(isset(json_decode($result)->estado)){
            $query = 'UPDATE PREFIX_order_carrier SET tracking_number = '.json_decode($result)->bultos[0]->numeroDeEnvio 
                . ' WHERE id_order = '.$params['id_order'];
            $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
            Db::getInstance()->execute(trim($query));
        }else{
            echo json_decode($result)->title;
        }     
    }
}
