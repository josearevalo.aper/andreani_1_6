<?php

class shippingmoduleDisplayCarrierListController
{
    public function __construct($module, $file, $path)
    {
        $this->file = $file;
        $this->module = $module;
        $this->context = Context::getContext();
        $this->_path = $path;
    }

    public function run($params)
    {
        $query = 'SELECT * FROM PREFIX_cart_product cp '
            . 'LEFT JOIN PREFIX_product p '
            . 'ON cp.id_product = p.id_product '
            . 'WHERE cp.id_cart = '.$params['cart']->id;
        $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
        $supplier = Db::getInstance()->executeS(trim($query))[0];
        $id_supplier = $supplier['id_supplier'];

        $query = 'SELECT * FROM PREFIX_envioandreani_supplier es '
        . 'WHERE es.id_supplier= '.$id_supplier;
        $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
        $el_supplier = Db::getInstance()->executeS(trim($query))[0];
        
        $query = 'SELECT * FROM PREFIX_carrier c JOIN PREFIX_envioandreani_carrier ec 
        on c.id_carrier = ec.id_carrier '
        . 'WHERE ec.name = \'A sucursal\' and c.deleted = 0';
        $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
        $asucursal = Db::getInstance()->executeS(trim($query))[0];
        $address = new Address($params['cart']->id_address_delivery);
        
        $ch = curl_init("https://api.andreani.com/v2/sucursales?seHaceAtencionAlCliente=true");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET"); 
        $sucursales = curl_exec($ch);
        curl_close($ch);
        $arrayprov = array();
        $arraysuc = array();
        foreach(json_decode($sucursales) as $sucursal){
            $key = $sucursal->descripcion . " (".$sucursal->direccion->calle . " " . $sucursal->direccion->numero.")";
            $val = $sucursal->direccion->codigoPostal.";".$sucursal->direccion->provincia.";".$sucursal->id;
            $arraysuc[$key] = $val;
            if($sucursal->direccion->provincia != '' && !in_array($sucursal->direccion->provincia, $arrayprov, true)){
                array_push($arrayprov, $sucursal->direccion->provincia);
            }
        }
        sort($arrayprov);
        ksort($arraysuc);
        
        if(!empty(Context::getContext()->cookie->prov_selected)){
            $this->context->smarty->assign('prov_selected', Context::getContext()->cookie->prov_selected);
        }else{
            $this->context->smarty->assign('prov_selected', "-1");
        }
        if(!empty(Context::getContext()->cookie->suc_selected)){
            $this->context->smarty->assign('suc_selected', Context::getContext()->cookie->suc_selected);
        }else{
            $this->context->smarty->assign('suc_selected', "-1");
        }
        if(!empty(Context::getContext()->cookie->id_carrier)){
            $this->context->smarty->assign('id_carrier', Context::getContext()->cookie->id_carrier);
            $this->context->smarty->assign('show_price', true);
        }else{
            $this->context->smarty->assign('id_carrier', $params['cart']->id_carrier);
            $this->context->smarty->assign('show_price', false);
        }
        $this->context->smarty->assign('id_a_sucursal', $asucursal['id_carrier']);
        $this->context->smarty->assign('provincias', $arrayprov);
        $this->context->smarty->assign('sucursales', $arraysuc);
        $this->context->smarty->assign('address', $address);
        //$this->context->smarty->assign('id_carrier', $params['cart']->id_carrier);
        $this->context->smarty->assign('id_supplier', $id_supplier);
        $this->context->smarty->assign('supplier', $el_supplier);
        $this->context->smarty->assign('id_cart', $params['cart']->id);
        $this->context->smarty->assign('kilos', Context::getContext()->cookie->kilos);
        
        return $this->module->display($this->file, '/views/hook/displayCarrierList.tpl');
    }
}
