<?php
/**
 * Created by IntelliJ IDEA.
 * User: gus
 * Date: 15/08/16
 * Time: 23:27
 */
use Andreani\Andreani;
use Andreani\Requests\CotizarEnvio;

class shippingmoduleGetOrderShippingCostController
{
    public function __construct($module, $file, $path)
    {
        $this->file = $file;
        $this->module = $module;
        $this->context = Context::getContext();
        $this->_path = $path;
    }

    public function run($cart, $shipping_cost)
    {
        $tarifa = 0;
        $kilos = 0;
        $query = 'SELECT * FROM PREFIX_cart_product cp '
        . 'WHERE cp.id_cart = '.$cart->id;
        $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
        $productos = Db::getInstance()->executeS(trim($query));
        $id_supplier = 0;
        foreach($productos as $producto):
            $query2 = 'SELECT * FROM PREFIX_product p '
            . 'WHERE p.id_product = '.$producto['id_product'];
            $query2 = str_replace('PREFIX_', _DB_PREFIX_, $query2);
            $prods = Db::getInstance()->executeS(trim($query2))[0];
            $id_supplier = $prods['id_supplier'];
            // echo var_dump($prods);
            if (count($prods) > 0) {
                $kilos = $kilos + ($prods['weight'] * $producto['quantity']);
            }
        endforeach;

        $query = 'SELECT * FROM PREFIX_envioandreani_supplier es '
        . 'WHERE es.id_supplier = '.$id_supplier;
        $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
        $proveedor = Db::getInstance()->executeS(trim($query))[0];

        Context::getContext()->cookie->__set('kilos', $kilos);
        if (empty(Context::getContext()->cookie->shipping_cost)) {
            $address = new Address($cart->id_address_delivery);

            $url = 'https://api.andreani.com/v1/tarifas';

            $data = array (
                'cpDestino' => $address->postcode,
                'contrato' => $proveedor['contract_standar'],
                'cliente' => $proveedor['number_client'],
                // 'sucursalOrigen' => 'BAR',
                'bultos[0][valorDeclarado]' => null,
                'bultos[0][volumen]' => null,
                'bultos[0][kilos]' => $kilos
                );
                
                $params = '';
            foreach($data as $key=>$value)
                        $params .= $key.'='.$value.'&';
                
                $params = trim($params, '&');
    
            $ch = curl_init();
    
            curl_setopt($ch, CURLOPT_URL, $url.'?'.$params ); //Url together with parameters
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
            curl_setopt($ch, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
            curl_setopt($ch, CURLOPT_HEADER, 0);
            
            $result = curl_exec($ch);
            curl_close($ch);
            if(isset(json_decode($result)->tarifaConIva)){
                $tarifa = json_decode($result)->tarifaConIva->total;
            }
        }else{
            $tarifa = Context::getContext()->cookie->shipping_cost;
        }
        return $tarifa;
    }
}
