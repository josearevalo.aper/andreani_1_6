<?php
/*
 *
 */

use Andreani\Andreani;
use Andreani\Requests\ConfirmarCompra;

class shippingmoduleOrderConfirmationController
{
    private $file;
    private $module;
    private $context;
    private $_path;

    public function __construct($module, $file, $path)
    {
        $this->file = $file;
        $this->module = $module;
        $this->context = Context::getContext();
        $this->_path = $path;
    }

    public function run($params)
    {
        $id_order = $params['objOrder']->id;
        $contract = Context::getContext()->cookie->contrato;
        if (!empty(Context::getContext()->cookie->suc_selected)) {
            $id_sucursal = Context::getContext()->cookie->suc_selected;
        }else{
            $id_sucursal = '';
        }
        $query = 'SELECT * FROM PREFIX_envioandreani_order eo '
        . 'WHERE eo.id_order = '.$id_order;
        $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
        $existe = Db::getInstance()->executeS(trim($query));
        if (count($existe) > 0) {
            $query = "UPDATE PREFIX_envioandreani_order 
        SET id_subsidiary = '".$id_sucursal."', number_contract = '".$contract."'
        WHERE `id_order` = '".$id_order."'";
        } else {
            $query = "INSERT INTO PREFIX_envioandreani_order (id_order, id_subsidiary, number_contract) "
        . "VALUES ('".$id_order."','".$id_sucursal."','".$contract."') ";
        }
        $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
        Db::getInstance()->execute(trim($query));

        Context::getContext()->cookie->__unset('id_carrier');
        Context::getContext()->cookie->__unset('contrato');
        Context::getContext()->cookie->__unset('shipping_cost');
        Context::getContext()->cookie->__unset('prov_selected');
        Context::getContext()->cookie->__unset('suc_selected');
    }
}
