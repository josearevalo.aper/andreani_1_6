<?php

class shippingmoduleetiquetaModuleFrontController extends ModuleFrontController {

    public function initContent()
	{
		parent::initContent();
        try {
        $id_supplier = $_REQUEST['id_supplier'];
        $nro_etiqueta = $_REQUEST['etiqueta'];
            
        $query = 'SELECT * FROM PREFIX_envioandreani_supplier es '
        . 'WHERE es.id_supplier = '.$id_supplier;
        $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
        $proveedor = Db::getInstance()->executeS(trim($query))[0];

        $url = 'https://api.andreani.com/login';

        $login = base64_encode($proveedor['user_supplier'].':'.$proveedor['pass_supplier']);
        $getRequest = array(
            'Authorization: Basic '.$login
        );
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET"); 
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $getRequest);

        $result = curl_exec($ch);
        //close cURL resource

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $headers = substr($result, 0, $header_size);
        $headers_arr = explode("\r\n", $headers);
        $token = str_replace("X-Authorization-token: ","",$headers_arr[3]);
        curl_close($ch);
        // echo var_dump($result);
        // die;

        //API URL
        $url = 'https://api.andreani.com/v2/ordenes-de-envio/'.$nro_etiqueta.'/etiquetas';
        //$url = 'https://api.andreani.com/v2/ordenes-de-envio/' . json_decode($result)->bultos[0]->numeroDeEnvio . '/etiquetas';

        //create a new cURL resource
        $ch = curl_init($url);


        //set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'x-authorization-token:'.$token));

        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_HEADER, 0); 

        //execute the POST request
        $etiqueta = curl_exec($ch);
        // print_r($etiqueta);die;
        header_remove();
        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="'.$nro_etiqueta.'.pdf"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        echo ($etiqueta);
      
        //close cURL resource
        curl_close($ch);

        } catch (PDOException $exception) {
            exit($exception->getMessage());
        }
        // $this->setTemplate('module:shippingmodule/views/templates/front/template_name.tpl');
	}
}