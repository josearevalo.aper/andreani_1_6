<?php

class shippingmoduleandreaniModuleFrontController extends ModuleFrontController {

    public function initContent()
	{
		parent::initContent();
        try {
            if($_SERVER['REQUEST_METHOD'] == "POST"){
                $id_pedido = $_REQUEST['id_pedido'];
                $estado = $_REQUEST['estado'];
                $query = "SELECT * FROM PREFIX_order_carrier oc LEFT JOIN PREFIX_orders o "
                . "ON oc.id_order = o.id_order WHERE oc.tracking_number = '".$id_pedido."'";
                $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
                $pedido = Db::getInstance()->executeS(trim($query))[0];
                $estadoNuevo = $pedido['current_state'];
                $estadoAnterior = $pedido['current_state'];
                $id_order = $pedido['id_order'];
                switch($estado){
                    case 'Distribucion':
                        $estadoNuevo = 4;
                        break;
                    case 'EnvioAnulado':
                        $estadoNuevo = 6;
                        break;
                    case 'EnvioEntregado':
                        $estadoNuevo = 5;
                        break;
                }
                $query = "UPDATE PREFIX_orders SET current_state = '".$estadoNuevo."' WHERE `id_order` = '".$id_order."'";
                $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
                if (!Db::getInstance()->execute(trim($query))) {
                    throw new Exception($this->module->displayError($this->module->l('Se produjo un error al guardar los datos')));
                }else{
                    if($estadoAnterior != $estadoNuevo){
                        date_default_timezone_set('America/Argentina/Buenos_Aires');
                        $query2 = "INSERT INTO `PREFIX_order_history` 
                        (`id_employee`, `id_order`, `id_order_state`, `date_add`) 
                        VALUES (0, ".$id_order.", ".$estadoNuevo.", '".date('Y-m-d H:i:s')."')";
                        $query2 = str_replace('PREFIX_', _DB_PREFIX_, $query2);
                        if (!Db::getInstance()->execute(trim($query2))) {
                            throw new Exception($this->module->displayError($this->module->l('Se produjo un error al guardar los datos')));
                        }else{
                            echo 'Los datos se guardaron con éxito';
                        }
                    }else{
                        echo 'Los datos se guardaron con éxito';
                    }
                }
            }
        } catch (PDOException $exception) {
            exit($exception->getMessage());
        }
        // $this->setTemplate('module:shippingmodule/views/templates/front/template_name.tpl');
	}
}