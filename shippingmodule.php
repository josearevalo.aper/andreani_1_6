<?php



if (!defined('_PS_VERSION_')) {
    exit;
}

class shippingModule extends Module
{
    public function __construct()
    {
        $this->name = 'shippingmodule';
        $this->tab = 'shipping_logistics';
        $this->version = '1.0.2';
        $this->author ='EnvioAndreani / 4r Soluciones';
        $this->need_instance = 0;
        $this->limited_countries = array('ar');
        $this->ps_versions_compliancy = array('min' => '1.6.x.x', 'max' => _PS_VERSION_);
        $this->bootstrap = true;
        $this->the_carriers = array('Estándar a domicilio', 'Urgentes a domicilio', 'A sucursal');

        parent::__construct(); //llamada al constructor padre.

        $this->displayName = $this->l('EnvioAndreani / 4r Soluciones'); // Nombre del módulo
        $this->description = $this->l('EnvioAndreani / 4r Soluciones'); //Descripción del módulo
        $this->confirmUninstall = $this->l('¿Estás seguro de que quieres desinstalar el módulo?'); //mensaje de alerta al desinstalar el módulo.

        $this->templateFile = 'module:shippingmodule/views/templates/hook/shippingmodule.tpl';
    }

    public function install()
    {
        if (!parent::install()) {
            return false;
        }

        // Install SQL tables
        $sql_file = dirname(__FILE__) . '/install/install.sql';
        if (!$this->loadSQLFile($sql_file)) {
            return false;
        }
        $this->addCarriers();
        if (!$this->registerHook('displayCarrierList')
            || !$this->registerHook('orderConfirmation')
            || !$this->registerHook('actionOrderStatusPostUpdate')
            || !$this->registerHook('ModuleRoutes')
            || !$this->registerHook('displayAdminOrderContentShip'))
            return false;
        return true;
    }

    public function hookDisplayAdminOrderContentShip($params)
    {
        // echo json_encode($params['order']);die;
        $id_order = $params['order']->id;

        $query = 'SELECT eo.*, od.product_weight, oc.tracking_number FROM PREFIX_envioandreani_order eo '
        . 'LEFT JOIN PREFIX_order_detail od '
        . 'ON od.id_order = eo.id_order '
        . 'LEFT JOIN PREFIX_order_carrier oc '
        . 'ON eo.id_order = oc.id_order '
        . 'WHERE eo.id_order = '.$id_order;
        $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
        $orden = Db::getInstance()->executeS(trim($query));
        
        $kilos = 0;
        $query = 'SELECT * FROM PREFIX_cart_product cp '
        . 'WHERE cp.id_cart = '.$params['order']->id_cart;
        $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
        $productos = Db::getInstance()->executeS(trim($query));
        $id_supplier = 0;
        foreach($productos as $producto):
            $query2 = 'SELECT * FROM PREFIX_product p '
            . 'WHERE p.id_product = '.$producto['id_product'];
            $query2 = str_replace('PREFIX_', _DB_PREFIX_, $query2);
            $prods = Db::getInstance()->executeS(trim($query2))[0];
            $id_supplier = $prods['id_supplier'];
            
            if (count($prods) > 0) {
                $kilos = $kilos + ($prods['weight'] * $producto['quantity']);
            }
        endforeach;
        if(isset($orden[0]) && $orden[0]['tracking_number'] != ''){
            return "<a target='_blank' href='/etiqueta?id_supplier=".$id_supplier."&etiqueta=".$orden[0]['tracking_number']."'>IMPRIMIR ETIQUETA</a>";
        }else{
            return null;
        }
    }

    public function hookModuleRoutes($params)
    {
        return [
            'shippingmodule-route-root' => [
                'rule' => 'andreani',
                'controller' => 'andreani',
                'keywords' => [
                    'id_pedido' => ['regexp' => '[0-9]+', 'param' => 'id_pedido']
                ],
                'params' => [
                    'fc' => 'module',
                    'module' => 'shippingmodule'
                ]
            ],
            'shippingmodule-route-root2' => [
                'rule' => 'etiqueta{-:id_supplier}{-:etiqueta}',
                'controller' => 'etiqueta',
                'keywords' => [
                    'id_supplier' => ['regexp' => '[0-9]+', 'param' => 'id_supplier'],
                    'etiqueta' => ['regexp' => '[0-9]+', 'param' => 'etiqueta']
                ],
                'params' => [
                    'fc' => 'module',
                    'module' => 'shippingmodule'
                ]
            ]
        ];
    }

    function hookOrderConfirmation($params)
    {
        $controller = $this->getHookController('orderConfirmation');
        return $controller->run($params);
    }

    public function hookActionOrderStatusPostUpdate($params)
    {
        $id_order_status = $params['newOrderStatus']->id;
        if(Module::isInstalled('riskevaluator') && Module::isEnabled('riskevaluator')){
            $id_state=26;
        }else{
            $id_state=2;
        }
        if($id_state == $id_order_status){
            $controller = $this->getHookController('actionOrderStatusPostUpdate');
            return $controller->run($params);
        }
        return null;
    }

    public function getHookController($hook_name)
    {
        require_once(dirname(__FILE__) . '/controllers/hook/' . $hook_name . '.php');
        $mod_name = $this->name;
        $controller_name = $mod_name . $hook_name . 'Controller';
        $controller = new $controller_name($this, __FILE__, $this->_path);
        return $controller;
    }

    public function hookDisplayCarrierList($params)
    {
        $controller = $this->getHookController('displayCarrierList');
        return $controller->run($params);
    }

    public function addCarriers()
    {
        foreach($this->the_carriers as $the_carrier){
            $carrier = new Carrier();
            $carrier->name = $the_carrier;
            $carrier->id_tax_rules_group = 0;
            $carrier->active = true;
            $carrier->deleted = false;
            $carrier->url = '';
            $carrier->delay = array();
            $carrier->shipping_handling = false;
            $carrier->range_behavior = 0;
            $carrier->is_module = true;
            $carrier->shipping_external = true;
            $carrier->external_module_name = $this->name;
            $carrier->need_range = true;
            $languages = Language::getLanguages(true);
            foreach ($languages as $language)
            {
                $carrier->delay[(int)$language['id_lang']] = $this->name;
            }
            $preGroups = Group::getGroups(Configuration::get('PS_LANG_DEFAULT'));
            $groups = array();
            foreach ($preGroups as $pre) {
                $groups[] = $pre['id_group'];
            }
            $rangePrice = new RangePrice();
            $rangePrice->delimiter1 = '0';
            $rangePrice->delimiter2 = '10000';
            $rangeWeight = new RangeWeight();
            $rangeWeight->delimiter1 = '0';
            $rangeWeight->delimiter2 = '10000';
            if (!$carrier->add())
                return false;
            $carrier = new Carrier($carrier->id); 
            $query = "INSERT INTO PREFIX_envioandreani_carrier (id_carrier, name) "
                . "VALUES ('".$carrier->id."', '".$the_carrier."') ";
            $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
            Db::getInstance()->execute(trim($query));
            method_exists('Carrier', 'setGroups') ? $carrier->setGroups($groups) : $this->setCarrierGroups($carrier, $groups);
            $carrier->addZone(Country::getIdZone(Country::getByIso('AR')));
            $rangePrice->id_carrier = $rangeWeight->id_carrier = (int)$carrier->id;
            $this->carrier_reference = (int)$carrier->id_reference;
            $rangePrice->add();
            $rangeWeight->add();
            copy(_PS_MODULE_DIR_.$this->name.'/views/img/logo.jpg', _PS_SHIP_IMG_DIR_.'/'.(int)$carrier->id.'.jpg');
        };
        return true;
    }

    /**
       * Install methods
       */
    public function loadSQLFile($sql_file)
    {
        $sql_content = file_get_contents($sql_file);

        $sql_content = str_replace('PREFIX_', _DB_PREFIX_, $sql_content);
        $sql_requests = preg_split("/;\s*[\r\n]+/", $sql_content);

        $result = true;
        foreach ($sql_requests as $request) {
            if (!empty($request)) {
                $result &= Db::getInstance()->execute(trim($request));
            }
        }

        return $result;
    }

    public function getOrderShippingCost($params, $shipping_cost)
    {
        $controller = $this->getHookController('getOrderShippingCost');
        return $controller->run($params, $shipping_cost);
    }

    public function getContent()
    {
        $mensaje = '';
        if (isset($_POST['saveChange'])) {
            $query = 'SELECT * FROM PREFIX_envioandreani_supplier es '
            . 'WHERE es.id_supplier= '.$_POST['id_supplier'];
            $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
            $existe = Db::getInstance()->executeS(trim($query));
            if (count($existe) > 0) {
                $query = "UPDATE PREFIX_envioandreani_supplier 
                SET user_supplier = '".$_POST['username']."', pass_supplier = '".$_POST['password']."', number_client = '".$_POST['number_client']."' 
                , contract_standar = '".$_POST['contract_standar']."', contract_urgent = '".$_POST['contract_urgent']."', contract_subsidiary = '".$_POST['contract_subsidiary']."'
                , address_postcode = '".$_POST['postcode']."', address_city = '".$_POST['city']."', address_street = '".$_POST['street']."'
                , address_number = '".$_POST['number']."', id_subsidiary = '".$_POST['sucursal']."', supplier_name = '".$_POST['name']."', supplier_email = '".$_POST['email']."'
                WHERE `id_supplier` = '".$_POST['id_supplier']."'";
            } else {
                $query = "INSERT INTO PREFIX_envioandreani_supplier (id_supplier, user_supplier, pass_supplier, number_client, contract_standar, contract_urgent, contract_subsidiary,
                address_postcode, address_city, address_street, address_number, id_subsidiary, supplier_name, supplier_email) "
                . "VALUES ('".$_POST['id_supplier']."','".$_POST['username']."','".$_POST['password']."','".$_POST['number_client']."','".$_POST['contract_standar']."','".$_POST['contract_urgent']."','".$_POST['contract_subsidiary']."'
                ,'".$_POST['postcode']."','".$_POST['city']."','".$_POST['street']."','".$_POST['number']."','".$_POST['sucursal']."','".$_POST['name']."','".$_POST['email']."') ";
            }
            $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
            if (!Db::getInstance()->execute(trim($query))) {
                throw new Exception($this->module->displayError($this->module->l('Se produjo un error al guardar los datos')));
            }else{
                $mensaje = 'Los datos se guardaron con éxito';
            }
        }
        $ch = curl_init("https://api.andreani.com/v2/sucursales?seHaceAtencionAlCliente=true");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET"); 
        $sucursales = curl_exec($ch);
        curl_close($ch);
        $arraysuc = array();
        // echo "<script>console.log(".var_dump($sucursales).")</script>";
        foreach(json_decode($sucursales) as $sucursal){
            $key = $sucursal->descripcion . " (".$sucursal->direccion->calle . " " . $sucursal->direccion->numero.") - Prov: ".$sucursal->direccion->provincia;
            $val = $sucursal->id;
            $arraysuc[$key] = $val;
        }
        ksort($arraysuc);
        $selectsuc = '';
        foreach($arraysuc as $key => $val){
            $selectsuc = $selectsuc.'<option value="'.$val.'">'.$key.'</option>';
        }
        $query = 'SELECT s.name, s.id_supplier as \'id_prov\', es.* FROM PREFIX_supplier s '
            . 'LEFT JOIN PREFIX_envioandreani_supplier es '
            . 'ON s.id_supplier = es.id_supplier '
            . 'WHERE s.active=1';
        $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
        $suppliers = Db::getInstance()->executeS(trim($query));
        $table = '<table class="table"><thead><tr><th scope="col">Proveedor</th><th scope="col">Usuario</th><th scope="col">Contraseña</th><th scope="col">Numero cliente</th><th scope="col">Standar a domicilio</th><th scope="col">Urgente a domicilio</th><th scope="col">A sucursal</th>
        <th scope="col">Ciudad</th><th scope="col">Direccion</th><th scope="col">Sucursal</th><th scope="col">Nombre</th><th scope="col">Email</th><th scope="col">Acciones</th></tr></thead><tbody>';
        foreach ($suppliers as $supplier) {
            $clave = array_search($supplier['id_subsidiary'], $arraysuc);
            $table .= '
            <tr>
              <td scope="row">' . $supplier['name'] . '</td>
              <td scope="row">' . $supplier['user_supplier'] . '</td>
              <td scope="row">' . $supplier['pass_supplier'] . '</td>
              <td scope="row">' . $supplier['number_client'] . '</td>
              <td scope="row">' . $supplier['contract_standar'] . '</td>
              <td scope="row">' . $supplier['contract_urgent'] . '</td>
              <td scope="row">' . $supplier['contract_subsidiary'] . '</td>
              <td scope="row">' . $supplier['address_city'] . ' (CP:'.$supplier['address_postcode'].')' . '</td>
              <td scope="row">' . $supplier['address_street'] . " " . $supplier['address_number'] . '</td>
              <td scope="row">' . $clave . '</td>
              <td scope="row">' . $supplier['supplier_name'] . '</td>
              <td scope="row">' . $supplier['supplier_email'] . '</td>
              <td scope="row">
              <button type="button" data-toggle="modal" data-target="#modalEditar" onclick="return cargarDatos('.str_replace('"',"'",json_encode($supplier)).');" class="btn btn-primary btn-xs">Editar</button>
              </td>
            </tr>';
        }
        $table .= '</tbody>
        </table>';

        $output = '
        <div class="panel">
            <div class="panel-heading">
                Cofiguración
            </div>
            <div class="alert alert-info">
            Configure los valores de las credenciales para cada uno de los proveedores.
            </div>';
            if($mensaje != ''){
                $output .= '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.
                    $mensaje
                .'</div>';
            }
            $output .= '<form class="defaultForm form-horizontal envioandreani" method="post" enctype="multipart/form-data">
                <div class="form-wrapper">'.
                $table
                .'</div><!-- /.form-wrapper -->
                <div class="modal fade" id="modalEditar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                      <button type="button" class="close" style="font-size:30px" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                        <h3 class="modal-title" id="exampleModalLabel">Modal title</h3>
                      </div>
                      <div class="modal-body">
                        <input type="hidden" id="id_supplier" name="id_supplier"/>
                        <h3 class="modal-title">Datos acceso</h3>
                        <div class="row">
                            <div class="col-lg-6 form-group">
                            <label for="username">Usuario</label>
                            <input type="text" id="username" name="username" required/>
                            </div>
                            <div class="col-lg-6 form-group">
                            <label for="username">Contraseña</label>
                            <input type="password" id="password" name="password" required/>
                            <input type="checkbox" id="check" onclick="myFunction()">Mostrar Contraseña
                            </div>
                        </div>
                        <hr>
                        <h3 class="modal-title">Datos de cliente y contratos</h3>
                        <div class="row">
                            <div class="col-lg-6 form-group">
                            <label for="number_client">Numero cliente</label>
                            <input type="text" id="number_client" name="number_client" required/>
                            </div>
                            <div class="col-lg-6 form-group">
                            <label for="contract_standar">Contrato standar a domicilio</label>
                            <input type="text" id="contract_standar" name="contract_standar"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 form-group">
                            <label for="contract_urgent">Contrato urgente a domicilio</label>
                            <input type="text" id="contract_urgent" name="contract_urgent"/>
                            </div>
                            <div class="col-lg-6 form-group">
                            <label for="contract_subsidiary">Contrato a sucursal</label>
                            <input type="text" id="contract_subsidiary" name="contract_subsidiary"/>
                            </div>
                        </div>
                        <hr>
                        <h3 class="modal-title">Dirección</h3>
                        <div class="row">
                            <div class="col-lg-6 form-group">
                            <label for="city">Ciudad</label>
                            <input type="text" id="city" name="city" required/>
                            </div>
                            <div class="col-lg-6 form-group">
                            <label for="postcode">Codigo postal</label>
                            <input type="text" id="postcode" name="postcode" required/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 form-group">
                            <label for="street">Calle</label>
                            <input type="text" id="street" name="street" required/>
                            </div>
                            <div class="col-lg-6 form-group">
                            <label for="number">Numero</label>
                            <input type="text" id="number" name="number" required/>
                            </div>
                        </div>
                        <hr>
                        <h3 class="modal-title">Proveedor</h3>
                        <div class="row">
                            <div class="col-lg-6 form-group">
                            <label for="name">Nombre proveedor</label>
                            <input type="text" id="name" name="name" required/>
                            </div>
                            <div class="col-lg-6 form-group">
                            <label for="email">Email proveedor</label>
                            <input type="text" id="email" name="email" required/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 form-group">
                            <label for="sucursal">Sucursal</label>
                            <select id="sucursal" name="sucursal">
                            <option value="-1">Seleccione</option>
                            '.$selectsuc.'
                            </select>
                            </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" id="saveChange" name="saveChange" class="btn btn-primary">Guardar</button>
                      </div>
                    </div>
                  </div>
                </div>
            </form>
        </div>
        <script>
        function cargarDatos(supplier){
            console.log(supplier);
          document.getElementById("password").type = "password";
          document.getElementById("check").checked = false;
          document.getElementById(\'id_supplier\').value = supplier.id_prov;
          document.getElementById(\'exampleModalLabel\').innerHTML = "CAMBIAR DATOS DE: " + supplier.name;
          document.getElementById(\'username\').value = supplier.user_supplier;
          document.getElementById(\'password\').value = supplier.pass_supplier;
          document.getElementById(\'number_client\').value = supplier.number_client;
          document.getElementById(\'contract_standar\').value = supplier.contract_standar;
          document.getElementById(\'contract_urgent\').value = supplier.contract_urgent;
          document.getElementById(\'contract_subsidiary\').value = supplier.contract_subsidiary;
          document.getElementById(\'city\').value = supplier.address_city;
          document.getElementById(\'street\').value = supplier.address_street;
          document.getElementById(\'postcode\').value = supplier.address_postcode;
          document.getElementById(\'number\').value = supplier.address_number;
          document.getElementById(\'sucursal\').value = supplier.id_subsidiary;
          document.getElementById(\'name\').value = supplier.supplier_name;
          document.getElementById(\'email\').value = supplier.supplier_email;
        };
        function myFunction() {
            var x = document.getElementById("password");
            if (x.type === "password") {
              x.type = "text";
            } else {
              x.type = "password";
            }
          }
        </script>
        <style>
        .table tbody>tr>td {
            font-size: 15px !important;
            padding: 13px 7px !important;
        }
        .bootstrap .form-group{
            margin-left:0px !important;
            margin-right:0px !important;
        }
        </style>
        ';
        return $output;
    }

    public function hookDisplayHeader($params)
    {
        $this->context->controller->registerStylesheet('modules-shippingmodule', 'modules/'.$this->name.'/views/css/shippingmodule.css', ['media' => 'all', 'priority' => 150]);
        $this->context->controller->registerJavascript('modules-shippingmodule', 'modules/'.$this->name.'/views/js/shippingmodule.js', [ 'position' => 'bottom','priority' => 150]);
    }

    public function uninstall()
    {
        $this->_clearCache('*');

        if (!parent::uninstall()) {
            return false;
        }

        $sql_file = dirname(__FILE__) . '/install/uninstall.sql';
        if (!$this->loadSQLFile($sql_file)) {
            return false;
        }
        $this->deleteCarriers();
        return true;
    }

    public function deleteCarriers()
    {

        foreach ($this->the_carriers as $the_carrier) {
            $query = 'SELECT * FROM PREFIX_carrier c WHERE c.name = \''.$the_carrier . '\'';
            $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
            $concidentes = Db::getInstance()->executeS(trim($query));
            foreach ($concidentes as $concidente) {
                $query = 'UPDATE PREFIX_carrier c SET c.deleted = 1 WHERE c.id_reference = '. $concidente['id_reference'];
                $query = str_replace('PREFIX_', _DB_PREFIX_, $query);
                Db::getInstance()->execute(trim($query));
            }
        }
        return true;
    }

    public function hookDisplayHome()
    {
        return $this->display(__FILE__, 'views/templates/hook/shippingmodule.tpl');
    }
}
